Pod::Spec.new do |s|
  s.name         = "MXPodSDK" # 项目名称
  s.version      = "2.3.4"        # 版本号 与 你仓库的 标签号 对应
  s.license      = "MIT"          # 开源证书
  s.summary      = "MXPodSDK for cocoaPods" # 项目简介

  s.homepage     = "https://gitee.com/sands.yu/MXPodSDK" # 你的主页
  s.source       = { :git => "https://gitee.com/sands.yu/MXPodSDK.git", :tag => "#{s.version}" }#你的仓库地址，不能用SSH地址
  s.source_files  = "MoxieSDK/**/*.{a,h,m}" # 你代码的位置
  s.requires_arc = true # 是否启用ARC
  s.platform     = :ios, "8.0" #平台及支持的最低版本
  s.resource     = 'MoxieSDK/resources/*'
  s.libraries    = "z"
  s.vendored_libraries = "MoxieSDK/libMoxieSDK.a"

  # User
  s.author       = { "sandsyu" => "yushengchu@aliyun.com" } # 作者信息
  
end